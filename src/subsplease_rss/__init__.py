"""The SubsPlease RSS helper."""
from importlib.metadata import version

__version__ = version(__name__)
