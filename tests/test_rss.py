"""Test cases for the SubsPleaseRSS class."""
import pytest

from subsplease_rss.rss import SubsPleaseRSS


def test_choose_quality() -> None:
    """It uses specified quality."""
    quality = SubsPleaseRSS(quality="720").choose_quality("720")
    assert quality == "720"


def test_wrong_quality() -> None:
    """It raises ValueError when quality is wrong."""
    with pytest.raises(ValueError):
        SubsPleaseRSS(quality="wrong")


def test_get_entries() -> None:
    """It returns any entries."""
    assert len(SubsPleaseRSS(quality="720").get_entries()) > 0


def test_title_extraction() -> None:
    """It returns anime title from RSS entry title."""
    data = "[SubsPlease] Build Divide - Code White - 21 (720p) [63A5366A]"
    extracted_title = SubsPleaseRSS().extract_title_and_episode(data)[0]
    assert extracted_title == "Build Divide - Code White"


def test_episode_extraction() -> None:
    """It returns anime episode from RSS entry title."""
    data = "[SubsPlease] Build Divide - Code White - 21 (720p) [63A5366A]"
    extracted_episode = SubsPleaseRSS().extract_title_and_episode(data)[1]
    assert extracted_episode == "21"


def test_parenthesis_episode_extraction() -> None:
    """It returns anime episde enclosed in parentheses from RSS entry title."""
    data = "[SubsPlease] Build Divide - Code White - (21-50) (720p) [63A5366A]"
    assert SubsPleaseRSS().extract_title_and_episode(data)[1] == "21-50"


def test_check_for_new_episode() -> None:
    """It returns True when there is new episode."""
    example_anime = SubsPleaseRSS().get_entries()[0]
    title, new_ep = SubsPleaseRSS().extract_title_and_episode(example_anime)
    old_ep = str(int(new_ep) - 1)
    anime_check = SubsPleaseRSS().check_for_new_episode(title, old_ep)
    assert anime_check == True


def test_check_for_new_episode_invalid_title() -> None:
    """It raises ValueError when provided anime title does not exist on RSS."""
    with pytest.raises(ValueError):
        SubsPleaseRSS().check_for_new_episode("Non existent anime", "01")


def test_check_for_new_episode_no_new_episode() -> None:
    """It returns False when there is not new episode."""
    example_anime = SubsPleaseRSS().get_entries()[0]
    title, new_ep = SubsPleaseRSS().extract_title_and_episode(example_anime)
    anime_check = SubsPleaseRSS().check_for_new_episode(title, new_ep)
    assert anime_check == False
